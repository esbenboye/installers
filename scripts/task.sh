#!/bin/sh
STATUS=$(task +ACTIVE export)
ID=$(echo "$STATUS" | jq -r '.[0].id')
DESCRIPTION=$(echo "$STATUS" | jq -r '.[0].description')
echo "$ID:$DESCRIPTION"
