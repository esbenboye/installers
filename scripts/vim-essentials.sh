curl https://bitbucket.org/esbenboye/installers/raw/master/configs/vimrc -fLo  ~/.vimrc --create-dirs
curl https://bitbucket.org/esbenboye/installers/raw/master/configs/monokai_ebj.vim -fLo ~/.vim/colors/monokai_ebj.vim --create-dirs
curl https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim -fLo ~/.vim/autoload/plug.vim --create-dirs
curl https://bitbucket.org/esbenboye/installers/raw/a2d2fb58d6859cc0641a878f1a2cedef31986c4e/configs/ackrc -fLo ~/.ackrc

#:PlugInstall
#:CocInstall coc-phpls
