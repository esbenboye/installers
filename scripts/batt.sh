#!/bin/sh

STATUS="$(pmset -g batt | grep -Eo "\d+%" | cut -d% -f1)"
DISCHARGING=$(pmset -g batt | grep -o ' discharging')
CHARGING=$(pmset -g batt | grep -o ' charging')
test "$CHARGING" = " charging" && STATUS="$STATUS+"
test "$DISCHARGING" = " discharging" && STATUS="$STATUS-"
echo "$STATUS"
