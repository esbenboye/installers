PATHS=$(cat ~/.tmux.sessions.json | jq -c ".[]")
for INFO in $PATHS; do
    NAME=$(echo $INFO | jq -cr ".name")
    DIR=$(echo $INFO | jq -cr ".path")
    tmux has-session -t $NAME || tmux new -d -s $NAME -c $DIR
done
