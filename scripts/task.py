#! /usr/bin/python3
from datetime import datetime, timedelta
import sys
import os
import json

filepath = "{}{}".format(os.getenv("HOME"), "/.tasks.txt")
defaultTimeFormat = "%Y-%m-%d %H:%M"

with open(filepath, "a"):
    pass

def logTask():
    taskStr = sys.argv[2]
    currentDateTime = (datetime.now().strftime(defaultTimeFormat))
    tasks = readJsonData()

    tasks['tasks'].append(
            {
                'start': currentDateTime,
                'end': '',
                'text': taskStr,
                'sent': ''
                }
            )
    writeJsonData(tasks)
    listTasks() 

def listTasks():
    tasks = readJsonData()
    tasks = tasks['tasks']
    index = 0

    if len(tasks) == 0:
        print('No tasks found')
    else:
        for task in tasks:
            start = datetime.strptime(task['start'], defaultTimeFormat)
            end = datetime.strptime(task['end'], defaultTimeFormat) if task['end']!="" else datetime.now()
            sent = "X" if task["sent"] == 1 else " "

            textDiff = getTextPresentationOfDiff(start, end)
            print("{} [{}] {}::{} ({}) --- {}".format(
                str(index).zfill(2),
                sent,
                start.strftime(defaultTimeFormat),
                end.strftime(defaultTimeFormat) if task['end']  != '' else " " * len(start.strftime(defaultTimeFormat)),
                textDiff,
                task['text'])
                )
            index += 1

def completeTask():
    completeIndex = int(sys.argv[2])
    updateField(completeIndex, "sent", 1)
    listTasks()

def endTask():
    completeIndex = int(sys.argv[2])
    currentDateTime = (datetime.now().strftime(defaultTimeFormat))
    updateField(completeIndex, "end", currentDateTime)
    listTasks()

def updateField(index, field, value):
    jsonData = readJsonData()

    if index > len(jsonData['tasks']) - 1 or index < 0:
        raise Exception("Out of bounds")

    jsonData['tasks'][index][field] = value
    writeJsonData(jsonData)

def removeTask():
    index = int(sys.argv[2])
    jsonData = readJsonData()
    del(jsonData['tasks'][index])
    writeJsonData(jsonData)
    listTasks()

def writeJsonData(jsonData):
    with open(filepath, "w") as outfile:
        json.dump(jsonData, outfile, indent=2)

def readJsonData(): 
    with open(filepath, "r") as handle:
        content = handle.read()
        jsonData = json.loads(content) if content != "" else {'tasks':[]}
        return jsonData

def getTextPresentationOfDiff(start, end):
    diff = (end - start).seconds

    hours = int(diff/(60*60))
    diff = diff - (hours * 60 * 60)

    minutes = int(diff/60)
    diff = diff - (minutes * 60)

    if(diff > 30):
        minutes = minutes + 1

    return "{}h {}m".format(str(hours).zfill(1), str(minutes).zfill(2))




action = sys.argv[1]
actions = {
    "list": listTasks,
    "start": logTask,
    "stop": endTask,
    "complete": completeTask,
    "remove":removeTask
}

actionMethod = actions.get(sys.argv[1])
actionMethod()
