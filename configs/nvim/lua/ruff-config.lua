require('lspconfig').ruff.setup {
  cmd = {"ruff", "server", "--preview"},
  init_options = {
    settings = {
      -- Any extra CLI arguments for `ruff` go here.
      args = {
      },
    }
  }
}

