require("mason").setup()
require("mason-lspconfig").setup({
    ensure_installed = {
        "intelephense",
    }
})

local lsp = require('lsp-zero').preset({})
lsp.on_attach(function(client, bufnr)
    lsp.default_keymaps({buffer = bufnr})
    vim.keymap.set("n", "<C-h>", function() vim.lsp.buf.hover() end)
    vim.keymap.set("n", "oo", function() vim.lsp.buf.definition() end)

end)

lsp.setup()

require('lspconfig').lua_ls.setup(lsp.nvim_lua_ls())
require("lspconfig").tailwindcss.setup({})

local cmp = require('cmp')

cmp.setup({
  mapping = {
    ['<CR>'] = cmp.mapping.confirm({select = false}),
  }
})
