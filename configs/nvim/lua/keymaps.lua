vim.keymap.set('n', '<leader>hw', function()
    local word = vim.fn.expand('<cword>')
    if word == "" then return end
    vim.cmd('match StatusLineTerm /\\<' .. word .. '\\>/')
end, { noremap = true, silent = false })

vim.keymap.set('n', '<leader>hc', ':match none<CR>', { noremap = true, silent = true })
