local nerdTreeStatuses = {
    Modified = 'M',
    Staged = 'S',
    Untracked = 'U',
    Renamed = '>',
    Unmerged = '=',
    Deleted = 'X',
    Dirty = 'x',
    Ignored = 'x',
    Clean = '+',
    Unknown = '?',
}

vim.g.NERDTreeMapOpenVSplit = '<C-v>'
vim.g.NERDTREEMapOpenSplit = '<C-i>'
vim.g.NERDTreeWinSize = 50
--vim.g.NERDTreeNodeDelimiter = "\u00a0"
vim.g.NERDTreeGitStatusConcealBrackets = 1

vim.g.NERDTreeGitStatusIndicatorMapCustom = nerdTreeStatuses

vim.g.NERDTreeMapOpenVSplit = '<C-v>'
vim.g.NERDTREEMapOpenSplit = '<C-i>'
