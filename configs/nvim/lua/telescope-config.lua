require('telescope').setup{
    defaults = {
        sorting_strategy = 'ascending',
        layout_config = {
            horizontal = {
                width = 0.80,
                height = 0.80,
                prompt_position = "top"
            }
        }
    },
    pickers = {
        buffers = {
            mappings = {
            }
        },
        lsp_references = {
            theme = "ivy"
        },
        lsp_definitions = {
            theme = "ivy"
        }
    },
    extensions = {
    }
}

require('telescope').load_extension('fzf')

