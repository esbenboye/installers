function! RunPhpmd()
    echo g:phpmd_standard
    set errorformat=%f:%l%m
    let l:filename=@%
    let l:phpmd_output=system('phpmd '.l:filename.' text '.g:phpmd_standard)
    let l:phpmd_list=split(l:phpmd_output, "\n")
    lexpr l:phpmd_list
    lopen
    "exec "nnoremap <silent> <buffer> q :ccl<CR>"
    "exec "nnoremap <silent> <buffer> t <C-W><CR><C-W>T"
    "exec "nnoremap <silent> <buffer> T <C-W><CR><C-W>TgT<C-W><C-W>"
    "exec "nnoremap <silent> <buffer> o <CR>"
    "exec "nnoremap <silent> <buffer> go <CR><C-W><C-W>"
    "exec "nnoremap <silent> <buffer> v <C-W><C-W><C-W>v<C-L><C-W><C-J><CR>"
    "exec "nnoremap <silent> <buffer> gv <C-W><C-W><C-W>v<C-L><C-W><C-J><CR><C-W><C-J>"
endfunction

command! Phpmd execute RunPhpmd()
