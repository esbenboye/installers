function! RunPhpcs()
    set errorformat=\"%f\"\\,%l\\,%c\\,%t%*[a-zA-Z]\\,\"%m\"\\,%*[a-zA-Z0-9_.-]\\,%*[0-9]\\,%*[0-9]
    let l:filename=@%
    lexpr system('phpcs --standard='.g:phpcs_standard.' '.l:filename.' --no-colors --report=csv | tail -n+2') 
    lopen
endfunction

command! Phpcs call RunPhpcs()
