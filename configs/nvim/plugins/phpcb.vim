function! RunPhpcb()
    silent %!phpcbf - --standard=PSR2
endfunction

command! Phpcb call RunPhpcb()

