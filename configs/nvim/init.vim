set rtp +=~/.vim
"https://github.com/junegunn/vim-plug
source ~/.config/nvim/plugins/phpmd.vim
source ~/.config/nvim/plugins/phpcb.vim
source ~/.config/nvim/plugins/phpcs.vim

call plug#begin('~/.vim/plugged')

Plug 'nvim-neotest/nvim-nio'
Plug 'rcarriga/nvim-dap-ui'
Plug 'nvim-telescope/telescope-dap.nvim', {'tag':'0.1.7'}

Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'scrooloose/nerdtree'
Plug 'preservim/nerdcommenter'
Plug 'adoy/vim-php-refactoring-toolbox'
Plug 'mfussenegger/nvim-dap'
"https://github.com/tpope/vim-surround
Plug 'tpope/vim-surround'
Plug 'itchyny/lightline.vim'
"https://github.com/junegunn/fzf.vim
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'majutsushi/tagbar'
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-repeat'
"Plug 'ludovicchabant/vim-gutentags'
"Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'wikitopian/hardmode'
"Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'mileszs/ack.vim'
Plug 'tobyS/vmustache'
Plug 'tobyS/pdv'
Plug 'OmniSharp/omnisharp-vim'
Plug 'kshenoy/vim-signature'
Plug 'szw/vim-maximizer'
Plug 'airblade/vim-gitgutter'
Plug 'Xuyuanp/nerdtree-git-plugin'
"Plug 'ap/vim-css-color'
"Plug 'leafOfTree/vim-svelte-plugin'

Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.4' }


Plug 'ThePrimeagen/vim-be-good'
Plug 'kdheepak/lazygit.nvim'
Plug 'voldikss/vim-floaterm'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plug 'williamboman/mason.nvim'
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'neovim/nvim-lspconfig'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" Autocompletion
Plug 'hrsh7th/nvim-cmp'     " Required
Plug 'hrsh7th/cmp-nvim-lsp' " Required
Plug 'L3MON4D3/LuaSnip'     " Required

Plug 'VonHeikemen/lsp-zero.nvim', {'branch': 'v2.x'}
Plug 'ThePrimeagen/harpoon'
Plug 'tpope/vim-dadbod'
Plug 'kristijanhusak/vim-dadbod-ui'
Plug 'kristijanhusak/vim-dadbod-completion'

Plug 'github/copilot.vim', {'branch': 'release'}


Plug 'github/copilot.vim', {'branch' : 'release'}

call plug#end()


let g:NERDCreateDefaultMappings = 1

lua require('harpoon-config')
lua require('ruff-config')
lua require('telescope-config')
lua require('treesitter-config')
lua require('lsp-config')
lua require('NERDTree-config')
lua require('intelephense-config')
lua require('php-dap')
lua require('jdtls-config')
autocmd FileType sql,mysql,plsql lua require('cmp').setup.buffer({ sources = {{ name = 'vim-dadbod-completion' }} })


let mapleader=","
let g:tagbar_autofocus=1
let g:pdv_template_dir = $HOME . "/workspace/esbenboye/installers/configs/pdv/templates"
let g:phpmd_standard = "PSR2"
let g:phpcs_standard = "PSR2"
let g:fzf_layout = { 'window' : { 'width': 0.8, 'height': 0.8 } }
let $FZF_DEFAULT_OPTS = '--reverse'
let NERDTreeIgnore=['__pycache__', '\~$']

lua require('keymaps')

augroup PhpCsStandard
    au! BufEnter */blite-web-shop/* let g:phpcs_standard = "PSR2"
    au! BufEnter */utime/* let g:phpcs_standard = "PSR2"
    au! BufEnter */factlines_github/* let g:phpcs_standard = "~/workspace/uptime/repositories/factlines_github/phpcs.xml"
augroup END

augroup PhpMdStandard
    au! BufEnter */factlines_github/* let g:phpmd_standard = "~/workspace/uptime/repositories/factlines_github/phpmd.xml"
augroup END

set foldcolumn=1
if has("vim")
    set term=xterm-256color
endif
set rnu
set nu
set tabstop=4
set shiftwidth=4
set expandtab
set cursorline
set incsearch
set hlsearch
set backspace=indent,eol,start
set nostartofline
set splitright
set splitbelow
set noswapfile
set colorcolumn=120
set ignorecase
set smartcase
set signcolumn=yes
hi Search guibg=LightBlue
syntax on
colorscheme monokai_ebj

nmap <C-k> 3k
nmap <C-j> 3j
nmap <C-f>   <cmd>Telescope find_files<cr>

nmap <F5>    <cmd>lua require('dap').continue()<cr>
nmap <F7>    <cmd>lua require('dap').step_over()<cr>
nmap <F8>    <cmd>Telescope lsp_document_symbols<CR>
nmap <F9>    <cmd>lua require('dap').step_out()<cr>

nmap <Space><Space> /<++><CR>ca<
nmap <C-l> :NERDTreeToggle<CR>
"nmap <silent><leader>d :call CocActionAsync('showSignatureHelp')<CR>
nmap <leader>ln :%s/<ln>/\=printf('%02d', line('.'))/<CR>
"nmap <leader>ft :set filetype?<CR>
nmap <leader>gu <cmd>Telescope lsp_references<cr>
" Coc Shortcuts
nmap <leader>fa :Ack!
nmap <leader>fw <cmd>Telescope grep_string<cr>
"nmap <leader>fc :CocSearch -i
nmap <leader>cs :Phpcs<CR>
nmap <leader>cb :Phpcb<CR>
nmap <leader>md :Phpmd<CR>
nmap <leader>nn :noh<CR>
nmap <leader>rd :redraw!<CR>
nmap <leader>nf :NERDTreeFind<CR>
nmap <leader>B  :lua require('dap').toggle_breakpoint()<CR>
nmap <leader>bc :bufdo bwipeout<CR>
nmap <leader>bl <cmd>Telescope buffers<CR>
nmap <leader>ml <cmd>Telescope marks<CR>
"nmap <leader>ri :CocCommand intelephense.index.workspace<CR>
nmap <tab> <cmd>lua vim.lsp.buf.code_action()<cr>
nmap <leader>rg <cmd>Telescope live_grep<CR>
nmap <leader>ds :%s/\s\+$//ce<CR>
nmap <leader>cn :cnext<CR>
nmap <leader>cp :cprev<CR>
"nmap <C-n> :lnext<CR>
nmap <leader>gb <cmd>lua require('telescope.builtin').git_branches {} <cr>
nmap <leader>hh <cmd>lua require('harpoon.ui').toggle_quick_menu()<cr>
nmap <leader>ha <cmd>lua require('harpoon.mark').add_file()<cr>
"nmap <leader>ca  <Plug>(coc-codeaction)
nmap <leader>cc <cmd>lua vim.lsp.buf.code_action()<cr>


nmap <leader>tt :FloatermNew<CR>
nmap <leader>ll :LazyGit<CR>

"
" Filename without extension
inoremap <Leader>fn <C-R>=expand("%:t:r")<CR>
nnoremap <Leader>fn <C-R>=expand("%:t:r")<CR>

" Filename with extension
inoremap <Leader>fe <C-R>=expand("%:t")<CR>
nnoremap <Leader>fe <C-R>=expand("%:t")<CR>

" Filename with absolute path
inoremap <Leader>fa <C-R>=expand("%:p:h")<CR>
nnoremap <Leader>fa <C-R>=expand("%:p:h")<CR>

" Filename with relative path
inoremap <Leader>fr <C-R>=expand("%:h")<CR>
nnoremap <Leader>fr <C-R>=expand("%:h")<CR>

nnoremap n nzz
nnoremap N Nzz

set clipboard=unnamedplus

augroup goFiles
    autocmd! * <buffer>
    autocmd FileType go nnoremap <C-H> :call <SID>show_documentation()<CR>

augroup END
nnoremap * *zz
nnoremap # #zz

" PHP documenter shortcuts
autocmd FileType php nnoremap <C-p> :call pdv#DocumentCurrentLine()<CR>

augroup cSharpFiles
    "autocmd! * <buffer>
    "autocmd FileType cs nnoremap <leader>rt :OmniSharpRunTest<CR>
    "autocmd FileType cs nnoremap <leader>ra :OmniSharpRunTestsInFile<CR>
    "autocmd FileType cs nnoremap <C-H> :OmniSharpDocumentation<CR>
    "autocmd FileType cs nnoremap oo :OmniSharpGotoDefinition<CR>
    "autocmd FileType cs nnoremap <leader>ca :OmniSharpGetCodeActions<CR>


    "let g:OmniSharp_selector_ui = 'fzf'
augroup END

augroup phpFiles
    autocmd! * <buffer>
    "autocmd FileType php nnoremap <C-H> :call <SID>show_documentation()<CR>
    autocmd FileType php nmap <silent> oo <cmd>Telescope lsp_definitions<cr>
augroup END


" let g:OmniSharp_popup_options = {
" \ 'padding': [1],
" \ 'border': [1]
" \}


" GitGutter Colors
highlight clear SignColumn
" LightLine config
set laststatus=2
set noshowmode
set mouse=

function! PhpDoc()
    call pdv#DocumentCurrentLine()
endfunction

set guicursor=n-v-c:block-Cursor

" Update time for GitGutter
set updatetime=100

let NERDTreeMapOpenVSplit='<C-v>'
let NERDTreeMapOpenSplit='<C-i>'

highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/

highlight StatusLineTerm ctermbg=red guibg=red
