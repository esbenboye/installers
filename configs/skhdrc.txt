meh - c : open -a "/Applications/Google Chrome.app"
meh - t : open -a "/Applications/iTerm.app"
meh - d : open -a "/Applications/TablePlus.app"
meh - o : open -a "/Applications/Obsidian.app"
meh - s : open -a "/Applications/Slack.app"
meh - q : /usr/local/bin/code ~/workspace/uptime/repositories/factlines_github

meh - l : osascript -e 'tell application "System Events" to keystroke "q" using {control down, command down}'
meh - r : osascript ~/workspace/esbenboye/installers/scripts/applescripts/chromeRefreshCurrentTab.applescript

meh - g : ~/workspace/esbenboye/installers/scripts/configure_tmux.sh

alt - c : cd ~/workspace/uptime/repositories/factlines_github/Svelte && npm run generate-components && cp ../g2/www/system/assets/js/dist/web-components.js ../factlines_github/www/system/assets/js/dist/web-components.js && terminal-notifier -message 'Compilation is done' -title 'NB!'

cmd - q : echo '' >> /dev/null
