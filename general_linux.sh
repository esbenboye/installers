
# Install cli-software
sudo apt install bash -yq
sudo apt install tmux -yq
sudo apt install vim -yq
sudo apt install zsh -yq
sudo apt install bash-completion -yq
sudo apt install gron -yq
sudo apt install ffmpeg  -yq
sudo apt install youtube-dl -yq
sudo apt install lolcal -yq
sudo apt install entr -yq
sudo apt install groff -yq
sudo apt install pandoc -yq
sudo apt install curl -yq
sudo apt install zathura -yq
sudo apt install screenkey -yq
sudo apt install ranger
# Install gui-based software
#brew cask install google-chrome 
#brew cask install iterm2 
#brew cask install vlc
#brew cask install sequel-pro 
#brew cask install sublime
#brew cask install phpstorm
#brew cask install sublime-text
#brew cask install spotify
#brew cask install moom
#brew cask install postman
#brew cask install mamp
#brew cask install docker
#brew cask install wkhtmltopdf
# Cleanup
#brew cleanup

# Copy vim config files and theme
curl https://raw.githubusercontent.com/sickill/vim-monokai/master/colors/monokai.vim -fLo ~/.vim/colors/monokai.vim --create-dirs
curl https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim -fLo ~/.vim/autoload/plug.vim --create-dirs 

# Config zsh 
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

ln -s ~/workspace/esbenboye/installers/configs/vimrc ~/.vimrc
ln -s ~/workspace/esbenboye/installers/configs/tmux.conf ~/.tmux.conf
ln -s ~/workspace/esbenboye/installers/configs/aliases ~/.aliases
ln -s ~/workspace/esbenboye/installers/configs/ssh_autocomplete.sh ~/.scripts

tmux source ~/.tmux.conf

sudo cat ./configs/zshrc >> ~/.zshrc

# Remember to enable git json_tool and osx in ~/.zshrc
# Remember to source ~/.aliases in ~/.zshrc

